/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#include "FileManager.h"

void changeWorkingDirectory(const Arguments *const arguments) {
    // Check the directory exists
    if (!arguments->workingDirectorySpecified) {
        return;
    }

    // Fatal error otherwise
    FATAL_ERROR_IF(!directoryExists(arguments->workingDirectory), "Error: Specified directory does not exist or inaccessible: %s\n", arguments->workingDirectory)

    // Change directory
    int result = chdir(arguments->workingDirectory);

    // Fatal error if failed
    FATAL_ERROR_IF(result != 0, "Error: Failed to change working directory to: %s reason: %s\n", arguments->workingDirectory, strerror(errno))
}

const char *selectBakeFile(const Arguments *const arguments) {
    // Check if specified the -f flags filename
    if (arguments->fileNameSpecified) {
        // Throw error if specified doesn't exist
        FATAL_ERROR_IF(!fileExistsAndReadable(arguments->fileName), "Error: Specified file not found or inaccessible: %s\n", arguments->fileName);

        // Else return
        return arguments->fileName;
    }

    // Check 'Bakefile'
    const char *bakeFile = "Bakefile";
    if (fileExistsAndReadable(bakeFile)) {
        return bakeFile;
    }

    // Check 'bakefile'
    bakeFile = "bakefile";
    if (fileExistsAndReadable(bakeFile)) {
        return bakeFile;
    }

    // Throw error
    FATAL_ERROR_IF(arguments->workingDirectorySpecified, "Error: Neither Bakefile or bakefile found or inaccessible in: %s\n", arguments->workingDirectory);
    FATAL_ERROR("Error: Neither Bakefile or bakefile found or inaccessible\n");
}

const char *readFileIntoBuffer(const char *const file, const char **const bufferEnd) {
    // Open file
    FILE *fileP = fopen(file, "r");
    FATAL_ERROR_IF(fileP == NULL, "Error: Failed to open file expected to be open-able\n");

    // Move 'cursor' to end
    int result = fseek(fileP, 0, SEEK_END);
    FATAL_ERROR_IF(result == -1, "Error: Failed to seek file\n")

    // Get 'cursor' position to find file size
    long size = ftell(fileP);
    FATAL_ERROR_IF(size == -1, "Error: Failed to 'ftell' file\n")

    // Move cursor back to start
    rewind(fileP);

    // Allocate room for characters and null byte
    char *buffer = malloc(sizeof(char) * (size + 1));
    MALLOC_ERROR_CHECK(buffer);

    // Sets the null byte
    buffer[size] = '\0';
    // Sets the buffer end pointer to the position of the null byte
    *bufferEnd = buffer + size;

    // Read into buffer, reporting error
    size_t read = fread(buffer, sizeof(char), (size_t) size, fileP);
    FATAL_ERROR_IF(read == 0 && !feof(fileP), "Error: Failed read from file: %s, error:%s\n", file, strerror(errno))

    return buffer;
}

bool fileExistsAndReadable(const char *const file) {
    // Simply pass of to more general function
    return fileExistsWithFlags(file, F_OK | R_OK);
}

bool fileExists(const char *const file) {
    // Simply pass of to more general function
    return fileExistsWithFlags(file, F_OK);
}

bool fileExistsWithFlags(const char *file, int flags) {
    // Try access
    int result = access(file, flags);

    // Check result
    if (result == 0) {
        return true;
    }

    errno = 0; // Silence any errors
    return false;
}

bool directoryExists(const char *const dir) {
    // Try open the directory
    DIR *directory = opendir(dir);

    // Check result
    if (directory != NULL) {
        closedir(directory);
        return true;
    }

    errno = 0; // Just silence the errors
    return false;
}

struct timespec getFileLastModifiedData(const char *file) {
    // Query the stats of the file, fatal error if didn't work
    struct stat fileStat;
    int result = stat(file, &fileStat);
    FATAL_ERROR_IF(result != 0, "Error: Failed to get stat for file expected to, error: %s", strerror(errno));

    // Return last modified field, linux/mac have different names for the field
#ifdef __linux__
    return fileStat.st_mtim;
#else
    return fileStat.st_mtimespec;
#endif
}
