/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#ifndef BAKE_FILE_MANAGER_H
#define BAKE_FILE_MANAGER_H

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <stdbool.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>

#include "Utility.h"
#include "ArgumentInterpreter.h"

/*
 * Checks the arguments struct to see if a working directory was specified and if
 * it was then, changes the current working directory to the one passed in. It also
 * uses 'directoryExists' to check if the directory exits first, and reports an
 * error if the directory doesn't exist.
 */
void changeWorkingDirectory(const Arguments *arguments);

/*
 * Checks the arguments struct to see if a file name was specified, and if it
 * was then checks to see if it exists and is readable, if it is returning
 * the file name. If an argument for file name was not specified then it
 * will first check for the file Bakefile' then 'bakefile' and return
 * the filename of the first found.
 *
 * If the specified filename is not found, or neither of the automatic ones
 * are found then it reports an error.
 */
const char *selectBakeFile(const Arguments *arguments);

/*
 * Opens the specified file and reads all the text into a buffer that is
 * just the right size, which it then returns a pointer to the start of,
 * which must be freed when done with. The bufferEnd pointer pass in is also
 * set to the point to the last byte of this buffer which should correspond
 * with the '\0' placed at the end of the buffer, making it a string.
 *
 * If the file is not open-able for any reason and error is reported.
 */
const char *readFileIntoBuffer(const char *file, const char **bufferEnd);

/*
 * Checks and returns whether or not the specific file both exists and is readable
 * by the program.
 */
bool fileExistsAndReadable(const char *file);

/*
 * Checks and returns whether or not the specific file exists
 */
bool fileExists(const char *file);

/*
 * Checks and returns where or not the specified file exits and the specified flags
 * are met.
 */
bool fileExistsWithFlags(const char *file, int flags);

/*
 * Checks and returns whether or not the specified folder exists.
 */
bool directoryExists(const char *dir);

/*
 * Gets and returns the timespec struct, containing the last modified date
 * of the file path passed it. If it fails to get information of the file
 * then it throws a fatal error.
 */
struct timespec getFileLastModifiedData(const char *file);

#endif //BAKE_FILE_MANAGER_H
