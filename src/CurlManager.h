/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#ifndef BAKE_CURL_MANAGER_H
#define BAKE_CURL_MANAGER_H

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#define __USE_XOPEN
#include <time.h>

#include "CommandManager.h"
#include "Utility.h"

#ifdef BAKE_CURL_MANAGER_CONSTANTS
    #define CURL_READ_BUFFER_SIZE 4096
#endif

/*
 * Given a command manager and a URL, it returns in secs the last modified date of
 * the file. Returns 0 if the URL is valid, but has no
 */
time_t getUrlLastModifiedDate(const CommandManager *commandManager, const char *URL);

#endif //BAKE_CURL_MANAGER_H
