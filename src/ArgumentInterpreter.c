	/* CITS2002 Project 2018/Volumes/USBDISK/src/TestBakefile
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#include "ArgumentInterpreter.h"

void setupArguments(Arguments *arguments) {
    memset(arguments, 0, sizeof(Arguments));
}

void interpretArguments(Arguments *arguments, int argc, char *const argv[]) {
    int c;

    // Just loop through all the argument, stopping at the first unexpected argument
    while ((c = getopt(argc, argv, "+f:C:inps")) != -1) {
        switch (c) {
            case 'f':
                // Specifying file name
                arguments->fileNameSpecified = true;
                if (arguments->fileName != NULL) {
                    // Free old one if it exists
                    free((void *) arguments->fileName);
                }
                // Duplicate argument
                arguments->fileName = strdup(optarg);
                break;

            case 'C':
                // Specifying working directory
                arguments->workingDirectorySpecified = true;
                if (arguments->workingDirectory != NULL) {
                    // Free old one if it exists
                    free((void *) arguments->workingDirectory);
                }
                // Duplicate argument
                arguments->workingDirectory = strdup(optarg);
                break;

            case 'i':
                arguments->ignoreErrors = true;
                break;

            case 'n':
                arguments->forcePrintOnly = true;
                arguments->executeSilently = false; //As -n and -s and not compatible
                break;

            case 'p':
                arguments->verbosePrintNoExecute = true;
                arguments->executeSilently = false; //As -p and -s and not compatible
                break;

            case 's':
                arguments->executeSilently = true;
                arguments->forcePrintOnly = false;          //As -s and -n and not compatible
                arguments->verbosePrintNoExecute = false;   //As -s and -p and not compatible
                break;

            case '?':
                exit(EXIT_FAILURE); //TODO more verbose error message?
            default:
            FATAL_ERROR("Error: Unexpected getopt return\n");
        }
    }

    if (optind == argc - 1) {
        // First non valid arg is the last, and thus is a target name

        arguments->targetNameSpecified = true;
        arguments->targetName = argv[optind];
    } else if (optind != argc) {
        // First non valid is not the last, and thus unexpected

        FATAL_ERROR("Error: Unexpected argument -- '%s'\n", argv[optind]); //TODO more verbose error message?
    }
}
