/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#include "VariableManager.h"
#include "StringInterpreter.h"

void setupVariableManager(VariableManager *const variableManager) {
    // Init the map
    setupUStrMap(&variableManager->variableUnorderedStringMap);

    // Add the special variables
    addSpecialVariable(variableManager, "RAND", SV_RAND);
    addSpecialVariable(variableManager, "PID", SV_PID);
    addSpecialVariable(variableManager, "PPID", SV_PPID);
    addSpecialVariable(variableManager, "PWD", SV_PWD);
}

void addSpecialVariable(VariableManager *const variableManager, const char *const name, const SpecialVariable specialVariable) {
    // Setup special variable and add it to the map
    SpecialMapValue specialMapValue;
    specialMapValue.nullChar = '\0';
    specialMapValue.specialVariable = specialVariable;
    setUStrMapElement(&variableManager->variableUnorderedStringMap, name, &specialMapValue, sizeof(SpecialMapValue));
}

void addVariableLine(VariableManager *variableManager, const LineInfo *lineInfo) {
    // Extract variable name and value from line info
    const char *variableName = strndup(lineInfo->firstSectionStart, lineInfo->firstSectionEnd - lineInfo->firstSectionStart + 1);
    const char *variableValue = strndup(lineInfo->secondSectionStart, lineInfo->secondSectionEnd - lineInfo->secondSectionStart + 1);

    // Check if it already exists and it is a special variable, if so don't add
    byte *currentVal;
    if ((currentVal = getUStrMapValue(&variableManager->variableUnorderedStringMap, variableName)) != NULL) {
        if (*currentVal == '\0') {
            // Starting with a null byte means that it is a special variable
            return;
        }
    }

    // Else just adds it to the map
    setUStrMapElementStr(&variableManager->variableUnorderedStringMap, variableName, variableValue);

    // Free the strings
    free((void *) variableName);
    free((void *) variableValue);
}

void makeVariableSubstitution(const VariableManager *const variableManager, const char **const line, size_t lineLength) {

    // Iterates through each character one at a time until the null byte
    for (const char *c = *line; *c != '\0'; ++c) {

        // Looks for a '$(X' pattern, where X is not '\0' and *c is at the '('
        if (*c == '(' && c > *line && *(c - 1) == '$' && *(c + 1) != '\0') {

            // Iterates through the characters starting at X
            for (const char *nextC = c + 1; nextC[0] != '\0'; ++nextC) {
                if (nextC[0] == '$' && nextC[1] == '(') {
                    // If it finds the start of another variable before a ) then
                    // advance *c up to the $ and leaves this loop to search for it's ')'

                    c = nextC;
                    break;
                } else if (nextC[0] == ')') {
                    // Found end of variable name

                    // Duplicate the string within the '(' and ')'
                    const char *stringToSearch = strndup(c + 1, nextC - c - 1);
                    STRDUP_ERROR_CHECK(stringToSearch);

                    // Search the variable map for a matching variable name
                    const byte *value = getUStrMapValue(&variableManager->variableUnorderedStringMap, stringToSearch);

                    // The string to substitute in when done.
                    const char *toSubIn = NULL;

                    if (value == NULL) {
                        // Variable not currently defined, so it's either environment variable or
                        // just not defined and should just be replace with emtpy string

                        // Check for environment variable, and set toSubIn appropriately
                        const char *envVar = getenv(stringToSearch);
                        if (envVar == NULL) {
                            toSubIn = strdup("");
                            STRDUP_ERROR_CHECK(toSubIn);
                        } else {
                            toSubIn = strdup(envVar);
                            STRDUP_ERROR_CHECK(toSubIn);
                        }
                    } else if (*value == '\0') {
                        // Variable value appears to be empty string, meaning that it is a special
                        // variable and needs to be dealt with specially

                        // Reinterpret memory as a SpecialMapValue struct
                        SpecialMapValue *specialMapValue = (SpecialMapValue *) value;

                        // Deal with each special case
                        switch (specialMapValue->specialVariable) {
                            case SV_RAND:
                                toSubIn = intToString(rand());
                                break;
                            case SV_PID:
                                toSubIn = intToString(getpid());
                                break;
                            case SV_PPID:
                                toSubIn = intToString(getppid());
                                break;
                            case SV_PWD:
                                toSubIn = getcwd(NULL, 0);
                                // Simple error check
                                FATAL_ERROR_IF(toSubIn == NULL, "Error: Unable to get current working directory\n")
                                break;
                        }
                    } else {
                        // Variable found and not special, so just copy the value from the map
                        toSubIn = strdup(value);
                        STRDUP_ERROR_CHECK(toSubIn);
                    }

                    // Length of string to copy in
                    size_t toSunInLen = strlen(toSubIn);

                    // The size of the string after making substitution
                    size_t newLineLength = lineLength - (nextC - c + 2) + toSunInLen;

                    // Copy current string, reallocate to fit new size and set the null byte
                    const char *newLine = strdup(*line);
                    STRDUP_ERROR_CHECK(newLine);
                    newLine = realloc((void *) newLine, newLineLength + 1);
                    REALLOC_ERROR_CHECK(newLine);
                    memset((void *) (newLine + newLineLength), '\0', 1);

                    // Copy toSubIn and the end of the old line into place
                    memcpy((void *) (newLine + ((c - 1) - *line)), toSubIn, toSunInLen);
                    memcpy((void *) (newLine + ((c - 1) - *line) + toSunInLen), nextC + 1, lineLength - (nextC - *line + 1));

                    // Free old line and redirection the line pointer passed in
                    free((void *) *line);
                    *line = newLine;

                    // free the string to be subbed in, and the one that was used to search
                    free((void *) toSubIn);
                    free((void *) stringToSearch);

                    // Change line length and move *c back to the start while exiting this loop
                    // to search for the next variable to be subbed in
                    lineLength = newLineLength;
                    c = *line;
                    break;

                }
            }
        }
    }
    // If it reaches here then it swept through the entire string without finding
    // any more variables to sub in and this is done with the line
}

const char *intToString(int value) {
    // Uses snprintf with NULL string to find out what size the string needs to be
    int size = snprintf(NULL, 0, "%d", value);

    // Allocate needed memory, and error check
    const char *string = malloc(sizeof(char) * (size + 1));
    MALLOC_ERROR_CHECK(string);

    // Convert the actual int into a string
    sprintf((char *) string, "%d", value);

    return string;
}

void cleanUpVariableManager(VariableManager *variableManager) {
    // Just cleans up the map
    cleanUpUStrMap(&variableManager->variableUnorderedStringMap);
}
