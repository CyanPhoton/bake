/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#include "TargetManager.h"

void setupTargetManager(TargetManager *const targetManager) {
    setupUStrMap(&targetManager->targetMap);
    targetManager->defaultTarget = NULL;
}

const char *handleTargetLine(TargetManager *targetManager, const VariableManager *variableManager, const char **bufferPointer, const char *bufferEnd, const LineInfo *lineInfo, LineInfo *unneededLineInfo, size_t *unneededLineLength) {
    // Copy out target name
    const char *target = strndup(lineInfo->firstSectionStart, lineInfo->firstSectionEnd - lineInfo->firstSectionStart + 1);

    const char *dependencies = NULL;
    if (lineInfo->secondSectionStart != NULL) {
        // If there are dependencies, copy the list out
        dependencies = strndup(lineInfo->secondSectionStart, lineInfo->secondSectionEnd - lineInfo->secondSectionStart + 1);
    }

    // Add the target, and dependency, getting the pointer to the pointer vector of actions to add to
    PointerVector *actionVector = addTarget(targetManager, target, dependencies);

    while (*bufferPointer != bufferEnd) {
        // Read all remaining lines from buffer, until a reason to stop

        // Read in the next line, advancing buffer pointer
        size_t nextLineLength;
        const char *nextLine = readNextLineFromBuffer(bufferPointer, &nextLineLength);

        if (nextLine == NULL) {
            // The line was empty, or no more lines, so just continue
            continue;
        }

        // Make all current variable substitutions
        makeVariableSubstitution(variableManager, &nextLine, nextLineLength);

        // Interpret that new line
        LineInfo nextLineInfo;
        interpretLine(nextLine, &nextLineInfo, true	);

        if (nextLineInfo.lineType != LT_ACTION && nextLineInfo.lineType != LT_COMMENT) {
            // The line was not an action or comment, or empty, so return unneeded
            // line as we are done here with getting actions, and that line needs
            // dealing with individually

            // Free strings, as returning early
            free((void *) target);
            free((void *) dependencies);

            // Pass back the unused info to be used in main.c
            *unneededLineInfo = nextLineInfo;
            *unneededLineLength = nextLineLength;
            return nextLine;

        } else if (nextLineInfo.firstSectionStart == NULL) {
            // The next line was empty, so just free and move on to next line

            free((void *) nextLine);
            continue;
        }
        // next line is an action line, so deal with by adding to vector

        TargetAction targetAction = createAction(&nextLineInfo);
        addVPElementCopy(actionVector, &targetAction, sizeof(TargetAction));

        free((void *) nextLine);
    }
    // Reached end of buffer, so just free what we got and return

    free((void *) target);
    free((void *) dependencies);
    return NULL;
}

PointerVector *addTarget(TargetManager *const targetManager, const char *const targetName, const char *const dependencies) {
    // Pointer to data in the unordered map
    UStrMapKeyValuePair *keyValuePair;
    Target *target;

    if ((keyValuePair = getUStrMapKeyValue(&targetManager->targetMap, targetName)) == NULL) {
        // Target not already in map, so add a new one

        // Add temp value to map
        Target targetTemp;
        setUStrMapElement(&targetManager->targetMap, targetName, &targetTemp, sizeof(Target));

        // Get pointer to the memory
        keyValuePair = getUStrMapKeyValue(&targetManager->targetMap, targetName);
        target = (Target *) keyValuePair->valuePointer;
    } else {
        // Target already in map, so cleanup old data

        // Print warning
        PRINT_MESSAGE("Warning: overriding target: %s\n", targetName);

        // Set pointer to the memory
        target = (Target *) keyValuePair->valuePointer;

        // Clean up old dependencies and actions
        cleanUpUStrSet(&target->dependencyUnorderedStringSet);
        for (PointerVectorIter p2 = target->actionVector.startIter; p2 != target->actionVector.endIter; ++p2) {
            TargetAction *targetAction = (TargetAction *) *p2;
            free((void *) targetAction->command);
        }
        cleanUpVectorPointer(&target->actionVector);
    }

    // Setup the target and dependency unordered set and action vector
    target->processed = false;
    target->wasBaked = true;
    setupUStrSet(&target->dependencyUnorderedStringSet);
    setupPointerVector(&target->actionVector);

    // Point the target name in the value to the one in the key, to not duplicate memory
    target->targetName = keyValuePair->key;

    if (targetManager->defaultTarget == NULL) {
        // Setting default if none it set, and thus is first target
        targetManager->defaultTarget = target;
    }

    if (dependencies != NULL) {
        // Check for it any dependencies specified

        // Keeps track of the start of the most recent dependency
        const char *depStart = dependencies;
        //Keeps track of the current character in the string
        const char *c;
        for (c = dependencies; *c != '\0'; ++c) {
            if ((*c == ' ' || *c == '\t') && depStart != NULL) {
                // If it's whitespace and was going over a dependency, then found
                // the end of a dependency

                // Copy dependency and place in vector, clearing start of dependency
                const char *dep = strndup(depStart, c - depStart);
                STRDUP_ERROR_CHECK(dep);
                addUStrSetString(&target->dependencyUnorderedStringSet, dep);
                free((void *) dep);
                depStart = NULL;

            } else if (*c != ' ' && *c != '\t' && depStart == NULL) {
                // If not whitespace and start of dependency not found, then
                // this is the start of the next dependency

                depStart = c;
            }
        }
        if (depStart != NULL) {
            // Dup last word if start was found
            const char *dep = strndup(depStart, c - depStart);
            STRDUP_ERROR_CHECK(dep);
            addUStrSetString(&target->dependencyUnorderedStringSet, dep);
            free((void *) dep);
        }
    }

    // Returns the action vector so it can be added to
    return &target->actionVector;
}

TargetAction createAction(const LineInfo *lineInfo) {
    // Create the struct
    TargetAction result;

    // Check the first character if the first section of the line
    // dealing with the cases where a special character is there
    switch (lineInfo->firstSectionStart[0]) {
        case '-':
            result.actionOption = AO_IGNORE_FAIL;
            break;

        case '@':
            result.actionOption = AO_SILENT;
            break;

        default:
            // Not special, so just so NONE and copy command out, returning

            result.actionOption = AO_NONE;
            result.command = strdup(lineInfo->firstSectionStart);
            return result;
    }
    // If got here, then special character was present, so select where command starts

    if (lineInfo->secondSectionStart != NULL && (lineInfo->firstSectionStart[1] == ' ' || lineInfo->firstSectionStart[1] == '\t')) {
        // Second section was found, and white space after special character,
        // so command starts at second section
        result.command = strdup(lineInfo->secondSectionStart);
    } else {
        // Else command starts in first section after special character
        result.command = strdup(lineInfo->firstSectionStart + 1);
    }

    return result;
}

Target *selectTargetToBake(const TargetManager *targetManager, const Arguments *arguments) {
    if (arguments->targetNameSpecified) {
        // Pointer to point to the data in map
        Target *target = NULL;

        if ((target = (Target *) getUStrMapValue(&targetManager->targetMap, arguments->targetName)) != NULL) {
            // Target name was specified and target name in map, just return

            return target;
        } else {
            // Target name was specified and target name is not is map, so throw fatal error

            FATAL_ERROR("Error: No rule for specified target: %s\n", arguments->targetName);
        }
    }
    // Target name not specified, so throw error if no default target
    FATAL_ERROR_IF(targetManager->defaultTarget == NULL, "Error: no targets\n");

    // Else just return default target;
    return targetManager->defaultTarget;
}

bool bakeTarget(const TargetManager *targetManager, const CommandManager *commandManager, const Arguments *arguments, Target *target) {
    // Set processed to keep track, so as to prevent circular dependencies
    // creating infinite loops
    target->processed = true;

    if (!targetNeedsBake(targetManager, commandManager, arguments, target)) {
        // Target does need baking
		
		//Reset processed, as we go back up the tree of dependencies
		target->processed = false;
		
		// Return false as did not need 
        return false;
    }
    // Getting here means need to bake target

	// Reset processed, as we go back up the tree of dependencies
	target->processed = false;
	
	// We are baking it, so set it so
    target->wasBaked = true;

    // Iterate through each action related to the target
    for (PointerVectorIter p = target->actionVector.startIter; p != target->actionVector.endIter; ++p) {
        // Get pointer to the action
        TargetAction *action = (TargetAction *) *p;

        if (arguments->verbosePrintNoExecute || arguments->forcePrintOnly || (!arguments->executeSilently && action->actionOption != AO_SILENT)) {
            // If one of the many flags is set so that you need to print the action
            // before running, or just to print it, then print it
            printf("%s\n", action->command);
        }
        if (!arguments->verbosePrintNoExecute && !arguments->forcePrintOnly) {
            // If one of the many flags is set so that you need to run the action
            // the execute the command by passing it to the shell
            int result = executeCommand(commandManager, action->command);

            if (result != 0 && (!arguments->ignoreErrors && action->actionOption != AO_IGNORE_FAIL)) {
                // If the command returned a NON zero exit code and one of the flags
                // is set so that it should fail on an error, then throw said fatal error
                FATAL_ERROR("Error: Action Failed: %s\n", action->command);
            }
        }
    }
    // Needed baking and did so, so true true

    return true;
}

bool targetNeedsBake(const TargetManager *targetManager, const CommandManager *commandManager, const Arguments *arguments, const Target *target) {
    // A vector of the dependencies
    PointerVector dependencyVector = getUStrSetStringVector(&target->dependencyUnorderedStringSet);

    if (dependencyVector.size == 0) {
        // If there are no dependencies then need to bake
        cleanUpUStrSetStringVector(&dependencyVector);
        return true;
    }

    // Struct to hold last modified data of target, along with a bool for returning
    struct timespec targetFileLastMod;
    bool needToBake = false;

    if (fileExists(target->targetName)) {
        // If file exists set the last modified date
        targetFileLastMod = getFileLastModifiedData(target->targetName);
    } else {
        // Else need to bake
        needToBake = true;
    }

    // Iterate through the dependencies
    for (PointerVectorIter p = dependencyVector.startIter; p != dependencyVector.endIter; ++p) {

        // Pointer to the second target if it is in the map
        Target *secondTarget = NULL;

        if ((secondTarget = (Target *) getUStrMapValue(&targetManager->targetMap, *p)) != NULL) {
            // If the target name is in the target map

            if (secondTarget->processed) {
                // If that dependency target has already been processed

                if (secondTarget->wasBaked) {
                    // If it was baked, then is needs to be back
                    needToBake = true;
                }

                // Report circular dependency and skip potentially bake
                PRINT_MESSAGE("Warning: ignoring circular dependency at: %s -> %s\n", target->targetName, secondTarget->targetName);
                continue;
            }

            if (bakeTarget(targetManager, commandManager, arguments, secondTarget)) {
                // No circular dependency and the the decency needed backing, so this does
                needToBake = true;
            }
        } else if (strncmp(*p, "file://", 7) == 0 || strncmp(*p, "http://", 7) == 0 || strncmp(*p, "https://", 8) == 0) {
            // Dependency starts with one of : "file://", "http://" or "https://" and thus it
            // a URL resource

            // Get last modified date of the URL file, and if doesn't have last modified the fatal error
            time_t depFileLastMod = getUrlLastModifiedDate(commandManager, *p);
            FATAL_ERROR_IF(depFileLastMod == 0, "Error: URL resource dependency does not have 'last modified date', and this is probably not a file: %s", (char *) *p);

            if (depFileLastMod > targetFileLastMod.tv_sec) {
                // URL dependency newer than target, so needs reBake
                needToBake = true;
            }
        } else if (fileExists(*p) && !needToBake) {
            // File exists and don't already need to Bake

            // Get the last modified data
            struct timespec depFileLastMod = getFileLastModifiedData(*p);

            if (depFileLastMod.tv_sec > targetFileLastMod.tv_sec || (depFileLastMod.tv_sec == targetFileLastMod.tv_sec && depFileLastMod.tv_nsec > targetFileLastMod.tv_nsec)) {
                // If dependency newer than target, need to make
                needToBake = true;
            }
        } else {
            // If target name not another target, or URL or local file, then needs Bake

            needToBake = true;
        }
    }

    // Just clean up vector, and return if need to Bake
    cleanUpUStrSetStringVector(&dependencyVector);
    return needToBake;
}

void cleanUpTargetManager(TargetManager *targetManager) {

    // Get vector of all target object
    PointerVector targets = getUStrMapPairVector(&targetManager->targetMap);

    // Cleanup each object
    for (PointerVectorIter p = targets.startIter; p != targets.endIter; ++p) {
        UStrMapKeyValuePair *keyValuePair = (UStrMapKeyValuePair *) *p;

        Target *target = (Target *) keyValuePair->valuePointer;
        // Doesn't clean up name as that memory belongs to the map, which is cleaned up after

        cleanUpUStrSet(&target->dependencyUnorderedStringSet);

        for (PointerVectorIter p2 = target->actionVector.startIter; p2 != target->actionVector.endIter; ++p2) {
            TargetAction *targetAction = (TargetAction *) *p2;
            free((void *) targetAction->command);
        }

        cleanUpVectorPointer(&target->actionVector);
    }
    cleanUpUStrMapPairVector(&targets);

    cleanUpUStrMap(&targetManager->targetMap);

}
