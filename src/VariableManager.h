/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#ifndef BAKE_VARIABLE_MANAGER_H
#define BAKE_VARIABLE_MANAGER_H

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <memory.h>
#include <errno.h>
#include <unistd.h>

#include "Utility.h"
#include "containers/UnorderedStringMap.h"
#include "StringInterpreter.h"

/*
 * An enum used to identify the different special variables, like RAND and PID
 */
enum SpecialVariable {
    SV_RAND,
    SV_PID,
    SV_PPID,
    SV_PWD
} typedef SpecialVariable;

/*
 * A struct that is stored in the map of variables for special variables.
 *
 * - It has a null byte at the front so that when interpreted as a string it
 *   has 0 length, an since no variable should equal nothing it allows them
 *   to be distinguished.
 * - The Special variable the stores the type.
 */
struct SpecialMapValue {
    char nullChar;
    SpecialVariable specialVariable;
} typedef SpecialMapValue;

/*
 * A variable manager struct to be passed to all variable manager functions to
 * provide them with al the data they need and to create an effective class.
 *
 * Just contains an unordered string map
 */
struct VariableManager {
    UStrMap variableUnorderedStringMap;
} typedef VariableManager;

/*
 * Just initialises the field of the VariableManager struct
 */
void setupVariableManager(VariableManager *variableManager);

/*
 * A simple function to help in init to add the special variables, taking the name
 * and the enum type
 */
void addSpecialVariable(VariableManager *variableManager, const char *name, SpecialVariable specialVariable);

/*
 * A function to add a variable name and value into the variable map, while checking
 * if it would override a special variable and preventing that.
 */
void addVariableLine(VariableManager *variableManager, const LineInfo *lineInfo);

/*
 * Using the variables found in the VariableManager struct and the environment
 * variables, check the line passed in and making any appropriate replacements needed.
 */
void makeVariableSubstitution(const VariableManager *variableManager, const char **line, size_t lineLength);

/*
 * A simply utility function to turn a int into a string, which will be malloc-ed
 * and thus needs to be freed.
 */
const char *intToString(int value);

/*
 * Cleans up any memory that might need to be freed from the VariableManager
 * struct
 */
void cleanUpVariableManager(VariableManager *variableManager);

#endif //BAKE_VARIABLE_MANAGER_H
