	/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#ifndef BAKE_MAIN_H
#define BAKE_MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>

#include "ArgumentInterpreter.h"
#include "FileManager.h"
#include "StringInterpreter.h"
#include "VariableManager.h"
#include "TargetManager.h"
#include "CommandManager.h"

Arguments arguments;
VariableManager variableManager;
TargetManager targetManager;
CommandManager commandManager;

const char *bakeFile;
/*
 * Set up bake to read and proccess the file
 */
void init(int argc, char *const argv[]);

/*
 * Read in the file and interpret all the lines
 */
void readBakefile(void);

/*
 * Prints the internal representation of the bake file
 */
void printInternalRepresentation(void);

/*
 * Proccess all the targets, dependencies and actions
 */
void processBakefile(void);

/*
 * Clean up any left over data
 */
void cleanUp(void);

/*
 * The main function, the entry point of the program
 */
int main(const int argc, char *const argv[]){

    // Get the start time of the program, to measure the duration of execution
    struct timeval startTime;
    gettimeofday(&startTime, NULL);

    // Initialise the structs, and needed data, then read in the bake file
    init(argc, argv);
    readBakefile();

    if (arguments.verbosePrintNoExecute){
        // If the -p flag was specified, print the internal representation of
        // the bake file that was read in, then clean up and exit
        printInternalRepresentation();

        cleanUp();

        // Get end time and print end message
        struct timeval endTime;
        gettimeofday(&endTime, NULL);
        printf("\nBake finished successfully in %.6f seconds !\n", (endTime.tv_sec - startTime.tv_sec) + (endTime.tv_usec - startTime.tv_usec) * 1.0e-6);

        return EXIT_SUCCESS;
    }
    // Didn't specify the -p flag, so proccess the file and then clean up and exit

    processBakefile();
    cleanUp();

    // Get end time and print end message
    struct timeval endTime;
    gettimeofday(&endTime, NULL);
    printf("\nBake finished successfully in %.6f seconds !\n", (endTime.tv_sec - startTime.tv_sec) + (endTime.tv_usec - startTime.tv_usec) * 1.0e-6);

    return EXIT_SUCCESS;
}

#endif //BAKE_MAIN_H
