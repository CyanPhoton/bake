/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#include "StringInterpreter.h"

const char *readNextLineFromBuffer(const char **const bufferPointer, size_t *const lineLength) {
    // If it is at the end, just return NULL
    if (**bufferPointer == '\0') {
        return NULL;
    }

    // The pointer that holds the line read in
    const char *line = NULL;

    // A pointer in the buffer, to where the last non whitespace or '\' before the
    // the new line char(s) is
    const char *lastNonWhiteSpace = NULL;
    // A pointer in the buffer, to where the last '\' before the the new line
    // char(s) is
    const char *lastBackslash = NULL;

    // Keeps track of current character
    const char *c;
    for (c = *bufferPointer; *c != '\0'; ++c) {

        if (c[0] != '\t' && c[0] != ' ' && c[0] != '\\' && c[0] != '\r' && c[0] != '\n') {
            // If not whitespace or '\' or some form of newline, update the pointer
            lastNonWhiteSpace = c;
            continue;
        }
        if (c[0] == '\\') {
            // If it is a '\' update the pointer
            lastBackslash = c;
            continue;
        }

        // The length of the line terminator
        int lineTerminatorLength = 0;
        if (c[0] == '\r' && c[1] == '\n') {
            // True for patter \r\n
            lineTerminatorLength = 1;
        }
        if (c[0] == '\r' || c[0] == '\n') {
            // True for \r or \n

            // Will end up as 1 in the case of \r or \n line terminator,
            // or 2 for \r\n
            lineTerminatorLength += 1;

            if (lastNonWhiteSpace == NULL && lastBackslash == NULL) {
                // This means the line is empty, so just
                // move the buffer pointer and return null

                *bufferPointer = c + lineTerminatorLength;
                return NULL;
            } else if (lastNonWhiteSpace == NULL) {
                // This means the line is just a '\', so to get it to just work,
                // 'pretend' the char before is not whitespace

                lastNonWhiteSpace = lastBackslash - 1;
            }

            if (c[-1] == '\\') {
                // If last character was '\' then this deals with appending

                // Duplicate the line, and advance the buffer pointer to the start of the next line
                *lineLength = lastNonWhiteSpace + 1 - *bufferPointer;
                line = strndup(*bufferPointer, *lineLength);
                STRDUP_ERROR_CHECK(line);
                *bufferPointer = c + lineTerminatorLength;

                // Get the next line
                size_t nextLineLength;
                const char *nextLine = readNextLineFromBuffer(bufferPointer, &nextLineLength);

                if (nextLine == NULL) {
                    // If the next line is NULL, then just return what is already got
                    return line;
                } else {
                    // True is there was whitespace before the '\' or at the start of the next line
                    bool hasSpace = (lastBackslash - lastNonWhiteSpace > 1) || nextLine[0] == ' ' || nextLine[0] == '\t';

                    // A pointer to the first non whitespace in that line
                    const char *firstNonWhitespace = NULL;

                    // Search for and set the first non whitespace
                    for (const char *c2 = nextLine; *c2 != '\0'; ++c2) {
                        if (c2[0] != ' ' && c2[0] != '\t') {
                            firstNonWhitespace = c2;
                            break;
                        }
                    }

                    // Increase the size of the line allocation to make room for the next line
                    size_t newLength = *lineLength + (hasSpace ? 1 : 0) + nextLineLength - (firstNonWhitespace - nextLine);
                    line = realloc((void *) line, sizeof(char) * (newLength + 1));
                    REALLOC_ERROR_CHECK(line);
                    if (hasSpace) {
                        // Places the space if needed
                        memset((void *) (line + *lineLength), ' ', 1);
                    }
                    // Copies the next line over
                    strcpy((char *) (line + *lineLength + (hasSpace ? 1 : 0)), firstNonWhitespace);

                    // Frees new line, sets length, then returns the line
                    free((void *) nextLine);
                    *lineLength = newLength;
                    return line;
                }
            } else if (lastBackslash != NULL && lastBackslash > lastNonWhiteSpace) {
                // Advances last non whitespace if last backslash is further on, but also
                // not the last character
                lastNonWhiteSpace = lastBackslash;
            }
            // Getting here means that the line ends normally with no append needed

            // Duplicate the line, set length, advance pointer and return
            *lineLength = lastNonWhiteSpace - *bufferPointer + 1;
            line = strndup(*bufferPointer, *lineLength);
            STRDUP_ERROR_CHECK(line);
            *bufferPointer = c + lineTerminatorLength;
            return line;
        }
    }
    // Getting here means that the line ends at the end of file

    if (c[-1] == '\\' && lastNonWhiteSpace == NULL) {
        // If the last line is just a '\', advance pointer and return NULL
        *bufferPointer = c;
        return NULL;
    }

    // Else just duplicate, set length, advance pointer and return line
    *lineLength = lastNonWhiteSpace - *bufferPointer + 1;
    line = strndup(*bufferPointer, *lineLength);
    STRDUP_ERROR_CHECK(line);
    *bufferPointer = c;
    return line;
}

void interpretLine(const char *const line, LineInfo *const lineInfo, const bool targetFound) {
    // Just initialises the LineInfo struct
    lineInfo->lineType = LT_INVALID;
    lineInfo->firstSectionStart = NULL;
    lineInfo->firstSectionEnd = NULL;
    lineInfo->secondSectionStart = NULL;
    lineInfo->secondSectionEnd = NULL;

    if (line[0] == '\t' && targetFound) {
        // Starting with a '\t' means it's an action line
        lineInfo->lineType = LT_ACTION;

        // A bool to help find start of second word
        bool foundSpace = false;

        // Search for location of first and last non whitespace of the
        // action to store in struct, as well as start of a second word if it exits
        for (const char *c = line; *c != '\0'; ++c) {
            if (c[0] != '\t' && c[0] != ' ') {
                if (lineInfo->firstSectionStart == NULL) {
                    lineInfo->firstSectionStart = c;
                    lineInfo->firstSectionEnd = c;
                } else {
                    lineInfo->firstSectionEnd = c;
                    if (foundSpace && lineInfo->secondSectionStart == NULL) {
                        lineInfo->secondSectionStart = c;
                    }
                }
            } else if (lineInfo->firstSectionStart != NULL) {
                foundSpace = true;
            }
        }

        return;
    }
    // If here then not an action

    // If true, searching for section left of '=' or ':'
    bool searchingFirstSection = true;
    // If true, while searching and found at least start, have found whitespace
    bool foundSpace = false;

    // Iterate through each character
    for (const char *c = line; *c != '\0'; ++c) {
        if (c[0] != '\t' && c[0] != ' ') {
            // If not whitespace

            if (c[0] == '#') {
                if (lineInfo->firstSectionStart == NULL) {
                    // If finds '#' and not yet found non whitespace, then must be comment line

                    lineInfo->lineType = LT_COMMENT;
                    return;
                }

                // Never the less, treat as '\0', since already not action line
                break;
            }

            if (c[0] == '=' && searchingFirstSection) {
                // A '=' means it is a variable, and are done searching for first section

                lineInfo->lineType = LT_VARIABLE;
                searchingFirstSection = false;
                continue;
            } else if (c[0] == ':' && searchingFirstSection) {
                // A ':' means it is a target, and are done searching for first section

                lineInfo->lineType = LT_TARGET;
                searchingFirstSection = false;
                continue;
            }

            if (lineInfo->firstSectionStart == NULL) {
                // Found non whitespace, and is first so set start, and end

                lineInfo->firstSectionStart = c;
                lineInfo->firstSectionEnd = c;
            } else if (searchingFirstSection) {
                if (foundSpace) {
                    // Still searching for first section and found whitespace, and now
                    // found non whitespace, and thus space in section on left side of
                    // ':' or '=' which makes the line invalid

                    lineInfo->lineType = LT_INVALID;
                    return;
                }
                // Else if line space not yet found, just update end of first section

                lineInfo->firstSectionEnd = c;
            } else if (lineInfo->secondSectionStart == NULL) {
                // If got to this point not looking for first section, and start of
                // second not yet found, so set start and end

                lineInfo->secondSectionStart = c;
                lineInfo->secondSectionEnd = c;
            } else {
                // Not searching first section, second section started, so update end

                lineInfo->secondSectionEnd = c;
            }
        } else if (searchingFirstSection && lineInfo->firstSectionStart != NULL) {
            // If found whitespace, while searching first section, and start of
            // first section is found, set that a space has been found

            foundSpace = true;
        }
    }

    if (lineInfo->firstSectionStart == NULL || (lineInfo->lineType == LT_VARIABLE && lineInfo->secondSectionEnd == NULL)) {
        // If no non whitespace ever found or it's a variable but nothing on the
        // right side of '=' the the line is invalid. Keep in mind that empty lines
        // already have been discarded before this point.

        lineInfo->lineType = LT_INVALID;
    }
}
