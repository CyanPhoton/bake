/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#define BAKE_CURL_MANAGER_CONSTANTS
#include "CurlManager.h"

time_t getUrlLastModifiedDate(const CommandManager *commandManager, const char *URL) {

    // File descriptors to store or values of stdout and stderr so they can be restored
    int stdout_bk = dup(STDOUT_FILENO);
    int stderr_bk = dup(STDERR_FILENO);

    // Create pipes to temporally replace stdout and stderr with
    int stdoutReplacementPipe[2];
    pipe(stdoutReplacementPipe);

    int stderrReplacementPipe[2];
    pipe(stderrReplacementPipe);

    // Swap out replacements and current
    dup2(stdoutReplacementPipe[1], STDOUT_FILENO);
    dup2(stderrReplacementPipe[1], STDERR_FILENO);

    // Construct command string for CURL
    int size = snprintf(NULL, 0, "curl %s --head -k", URL);
    char *command = malloc((size_t) size + 1);
    sprintf(command, "curl %s --head -k", URL);

    // Run curl command
    int exitCode = executeCommand(commandManager, command);
    if (exitCode != 0) {
        // Command exited with non 0 exit status, means error

        // Flush error and move stderr back in to print an error
        fflush(stderr);
        close(stderrReplacementPipe[1]);
        dup2(stderr_bk, STDERR_FILENO);

        // Open the stderr replacement to get curl's error
        FILE *fp = fdopen(stderrReplacementPipe[0], "r");

        // A fixed size buffer and setting null byte, as can't really read actual size
        char buf[CURL_READ_BUFFER_SIZE];
        buf[CURL_READ_BUFFER_SIZE-1] = '\0';

        // Read and spit to stderr all the contents
        while (!feof(fp)) {
            fgets(buf, CURL_READ_BUFFER_SIZE, fp);
            fprintf(stderr, "%s", buf);
        }

        // Throw error
        FATAL_ERROR("Error: URL resource dependency not available: %s\n", URL);
    }
    // No error

    // Free command string
    free(command);

    // Flush pipes and close unneeded
    fflush(stdout);
    fflush(stderr);
    close(stdoutReplacementPipe[1]);
    close(stderrReplacementPipe[0]);
    close(stderrReplacementPipe[1]);

    // Swap back the normal stdout and stderr
    dup2(stderr_bk, STDERR_FILENO);
    dup2(stdout_bk, STDOUT_FILENO);

    // Open stdout to be read in
    FILE *fp = fdopen(stdoutReplacementPipe[0], "r");

    // Again, fixed size buffer and set null byte
    char buf[CURL_READ_BUFFER_SIZE];
    buf[CURL_READ_BUFFER_SIZE - 1] = '\0';

    // Read in while pipe
    while (!feof(fp)) {
        // Read in line
        fgets(buf, CURL_READ_BUFFER_SIZE, fp);

        if (strncmp(buf, "Last-Modified:", 14) == 0) {
            // If line starts with "Last-Modified:", the read the time

            // Read in time and break it down
            struct tm brokenDownTime;
            strptime(buf + 15, "%a, %d %b %Y %H:%M:%S", &brokenDownTime);

            // Get timezone information
            time_t timer = time(NULL);
            struct tm timeInfo = {0};
            localtime_r(&timer, &timeInfo);

            // Cleanup remaining
            close(stdoutReplacementPipe[0]);
            fclose(fp);

            // Convert broken down time into seconds and add timezone offset
            return mktime(&brokenDownTime) + timeInfo.tm_gmtoff;
        }
    }
    // Getting here means that no "Last-Modified:" line was found

    // Clean up remaining
    close(stdoutReplacementPipe[0]);
    fclose(fp);

    // Return 0 for error
    return 0;
}
