/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#ifndef BAKE_TARGET_MANAGER_H
#define BAKE_TARGET_MANAGER_H

#include <stdlib.h>

#include "Utility.h"
#include "ArgumentInterpreter.h"
#include "FileManager.h"
#include "CommandManager.h"
#include "StringInterpreter.h"
#include "CurlManager.h"
#include "VariableManager.h"
#include "containers/UnorderedStringMap.h"
#include "containers/UnorderedStringSet.h"
#include "containers/PointerVector.h"

enum ActionOption {
    AO_NONE,
    AO_SILENT,      // '@'
    AO_IGNORE_FAIL  // '-'
} typedef ActionOption;

struct TargetAction {
    ActionOption actionOption;
    const char *command;
} typedef TargetAction;

/*
 * A struct to hold tha data for and represent a Target with its target name,
 * and lists of dependencies and actions.
 *
 * The processed bool is for the purpose of dealing with circular dependencies
 * that may arise
 *
 */
struct Target {
    const char *targetName;

    UStrSet dependencyUnorderedStringSet;
    PointerVector actionVector;

    bool processed;
    bool wasBaked;
} typedef Target;

/*
 * A struct to hold all the data that the TargetManager needs and to act as
 * a sort of class.
 */
struct TargetManager {
    Target *defaultTarget;
    UStrMap targetMap;
} typedef TargetManager;

/*
 * Just initialise the data in a TargetManager struct
 */
void setupTargetManager(TargetManager *targetManager);

/*
 * Extracts from a line the information needed for a target and its dependencies and
 * actions.
 */
const char *handleTargetLine(TargetManager *targetManager, const VariableManager *variableManager, const char **bufferPointer, const char *bufferEnd, const LineInfo *lineInfo, LineInfo *unneededLineInfo, size_t *unneededLineLength);

/*
 * Add a target to the data in the TargetManager struct, with the specified target
 * name and the dependencies in 1 string which will be split up automatically.
 *
 * It returns a pointer to a vector of the actions, so they can be added one by
 * one as they are read in from the buffer.
 */
PointerVector *addTarget(TargetManager *targetManager, const char *targetName, const char *dependencies);

/*
 * Given the lineinfo of an action line, create a TargetAction from it, taking
 * out either of the special characters that could appear at the start (- or @)
 */
TargetAction createAction(const LineInfo *lineInfo);

/*
 * Given the arguments passed in and the targets in the target manager, return a
 * pointer to the target in the map to start from. If the argument specified target
 * does not exist, or there is no target then exit with a fatal error.
 */
Target *selectTargetToBake(const TargetManager *targetManager, const Arguments *arguments);

/*
 * Given a target, check if it needs to be baked, and if it does then run the action and return
 * true for needed to be baked, else false if it didn't need to be baked. It will print the
 * command to be executed unless the appropriate flags have been set, and it will only print if the
 * appropriate flags have been set. It will also fatal error if the action returns with non 0 exit
 * code, unless the appropriate flags have been set.
 */
bool bakeTarget(const TargetManager *targetManager, const CommandManager *commandManager, const Arguments *arguments, Target *target);

/*
 * Given a target, check if it needs to be baked, and return if it does or not. It also will if needed
 * check and potentially bake it's dependencies, and if a URL resource is not available it will exit
 * with a fatal error.
 */
bool targetNeedsBake(const TargetManager *targetManager, const CommandManager *commandManager, const Arguments *arguments, const Target *target);

/*
 * Cleans up anything from the TargetManager struct that needs to be
 */
void cleanUpTargetManager(TargetManager *targetManager);

#endif //BAKE_TARGET_MANAGER_H
