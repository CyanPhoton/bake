/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#ifndef BAKE_STRING_INTERPRETER_H
#define BAKE_STRING_INTERPRETER_H

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "Utility.h"

/*
 * An enum to specify the different types of lines
 */
enum LineType {
    LT_INVALID,
    LT_VARIABLE,
    LT_TARGET,
    LT_ACTION,
    LT_COMMENT
} typedef LineType;

/*
 * A struct which holds all the information about a line
 * after it has been interpreted
 */
struct LineInfo {
    LineType lineType;

    const char *firstSectionStart, *firstSectionEnd;
    const char *secondSectionStart, *secondSectionEnd;
} typedef LineInfo;

/*
 * Takes by reference a pointer to the start of the next line to read from in the
 * buffer, reads in and returns the next line and advances the pointer to the
 * start of the next line, for the next call. It also will auto append the next
 * line if the current one ends in a \ and move the pointer accordingly. Also sets
 * the length of the line passed in, as strlen would return.
 */
const char *readNextLineFromBuffer(const char **bufferPointer, size_t* lineLength);

/*
 * Take a line and a LineInfo struct and interprets the line to finds it's type and
 * the location of important pars so it can be dealt with latter, setting those in
 * the struct. If a action has not yet been found, then
 */
void interpretLine(const char *line, LineInfo *lineInfo, bool targetFound);

#endif //BAKE_STRING_INTERPRETER_H
