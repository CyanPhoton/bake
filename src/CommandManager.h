/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#ifndef BAKE_COMMAND_MANAGER_H
#define BAKE_COMMAND_MANAGER_H

#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>

/*
 * A struct to hold the data the command manager needs, and acts as a class of sorts
 */
struct CommandManager {
    const char *shell;
} typedef CommandManager;

/*
 * Selects the path of the shell program to be used, via either the SHELL
 * environment variable, or defaulting to "/bin/bash"
 */
void selectCommandShell(CommandManager *commandManager);

/*
 * Using the selected shell program, executes the command via the -c option,
 * returning the exit code of the program
 */
int executeCommand(const CommandManager *commandManager, const char *command);

#endif //BAKE_COMMAND_MANAGER_H
