/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#ifndef BAKE_ARGUMENT_INTERPRETER_H
#define BAKE_ARGUMENT_INTERPRETER_H

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>

#include "Utility.h"

/*
 * A struct to hold all the results of what is read in from the command line
 * arguments, for ease of passing it around.
 */
struct Arguments { // test
    bool workingDirectorySpecified; // -C []
    const char *workingDirectory;

    bool fileNameSpecified; // -f []
    const char *fileName;

    bool ignoreErrors; //           -i
    bool forcePrintOnly; //         -n      conflicts with      -s
    bool verbosePrintNoExecute; //  -p      conflicts with      -s
    bool executeSilently; //        -s      conflicts with      -n and -p

    bool targetNameSpecified;
    const char *targetName;
} typedef Arguments;

/*
 * Just initialises the fields of the Argument struct passed in.
 */
void setupArguments(Arguments *arguments);

/*
 * When passed the argument vector and count, will read through each one a interpret
 * it's meaning and set the values in the Arguments struct to the appropriate values
 * from what it finds.
 *
 * It all reports to stderr any errors in the arguments it finds
 */
void interpretArguments(Arguments *arguments, int argc, char *const argv[]);

#endif //BAKE_ARGUMENT_INTERPRETER_H
