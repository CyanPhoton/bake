/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#include "CommandManager.h"

void selectCommandShell(CommandManager *commandManager) {
    // Check the "SHELL" environment variable
    commandManager->shell = getenv("SHELL");

    if (commandManager->shell == NULL) {
        // If "SHELL" environment variable not set, use default

        commandManager->shell = "/bin/bash";
    }
}

int executeCommand(const CommandManager *commandManager, const char *command) {
    // Create a child process and get return value
    int pid = fork();

    if (pid == 0) {
        // Is child process

        // Reset errno, and replace process image with that of the SHELL with the
        // command as an argument after the "-c" flag
        errno = 0;
        execl(commandManager->shell, getcwd(NULL, 0), "-c", command, (char *) NULL);

        // If execl has returned, then it failed, so exit this child proccess with
        // the return code of the SHELL
        exit(errno);
    }
    // Is parent proccess

    // Wait for child proccess to exit
    int status = 0;
    waitpid(pid, &status, 0);

    // Get and return the exit status of child process
    return WEXITSTATUS(status);
}
