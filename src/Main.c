/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#include "Main.h"

void init(const int argc, char *const argv[]) {
    // Set the messages to be prepended by the first argument
    SETUP_MESSAGE_SOURCE(argv[0]);

    // Setup structs, initialising fields
    setupArguments(&arguments);
    setupVariableManager(&variableManager);
    setupTargetManager(&targetManager);
    selectCommandShell(&commandManager);

    // Interpret arguments, change working directory if needed and select the bake file
    interpretArguments(&arguments, argc, argv);
    changeWorkingDirectory(&arguments);
    bakeFile = selectBakeFile(&arguments);
}

void readBakefile(void) {
    // Read in the file into a buffer, setting a pointer to the start, end and
    // another initially to the start to be moved along
    const char *bufferEnd;
    const char *buffer = readFileIntoBuffer(bakeFile, &bufferEnd);
    const char *bufferPointer = buffer;

    // Variables set to the information of a line if it is read in and not needed
    // so it doesn't need to be read again
    const char *unneededLine = NULL;
    size_t unneededLineLength = 0;
    LineInfo unneededLineInfo;
    memset(&unneededLineInfo, 0, sizeof(LineInfo));

    // Read lines until the end of the file, or while already read lines not dealt with
    while (bufferPointer != bufferEnd || unneededLine != NULL) {

        // Info for the current line
        const char *line;
        LineInfo lineInfo;
        size_t lineLength;

        if (unneededLine != NULL) {
            // The unneeded line was not null, so use that information,
            // then set it to null so it is not used again
            line = unneededLine;
            lineLength = unneededLineLength;
            lineInfo = unneededLineInfo;

            unneededLine = NULL;
        } else {
            // The unneeded was null, so read in a line
            line = readNextLineFromBuffer(&bufferPointer, &lineLength); // Will advance bufferPointer to start of next line

            if (line == NULL) {
                // If the line was empty, just move on to next line
                continue;
            }

            // Substitute into the line the current variables
            makeVariableSubstitution(&variableManager, &line, lineLength);

            // Interpret the line
            interpretLine(line, &lineInfo, false);
        }

        // Proccess the line depending on type
        switch (lineInfo.lineType) {
            case LT_VARIABLE:
                addVariableLine(&variableManager, &lineInfo);
                break;
            case LT_TARGET:
                unneededLine = handleTargetLine(&targetManager, &variableManager, &bufferPointer, bufferEnd, &lineInfo, &unneededLineInfo, &unneededLineLength);
                break;
            case LT_INVALID:
            FATAL_ERROR("Error: Invalid line: %s\n", line); //TODO maybe give more feedback?
            case LT_ACTION:
            FATAL_ERROR("Error: Unexpected action line: %s\n", line);
            case LT_COMMENT:
                break;
        }
        // Clean up the line
        free((void *) line);
    }

    // Clean up the buffer
    free((void *) buffer);
}

void printInternalRepresentation(void) {
    // Preface the output
    PRINT_MESSAGE("\n\n Bake Internal Representation: \n\n")

    // Get a pointer vector of all the variables
    printf("Variables: \n");
    PointerVector variables = getUStrMapPairVector(&variableManager.variableUnorderedStringMap);

    for (PointerVectorIter p = variables.startIter; p != variables.endIter; ++p) {
        // Iterate through each variable

        UStrMapKeyValuePair *keyValuePair = (UStrMapKeyValuePair *) *p;
        if (keyValuePair->valuePointer[0] == '\0') {
            // Ignore the special variables
            continue;
        }

        // Print the name and value
        printf("Variable Name: %s, Value: %s\n", keyValuePair->key, keyValuePair->valuePointer);
    }
    cleanUpUStrMapPairVector(&variables);

    // Get a pointer to all the targets
    printf("\nTargets:\n");
    PointerVector targets = getUStrMapPairVector(&targetManager.targetMap);

    for (PointerVectorIter p = targets.startIter; p != targets.endIter; ++p) {
        // Iterate through all the targets

        // Get target out of pair
        UStrMapKeyValuePair *keyValuePair = (UStrMapKeyValuePair *) *p;
        Target *target = (Target *) keyValuePair->valuePointer;

        // Get a pointer to the vector of dependencies
        PointerVector dependencies = getUStrSetStringVector(&target->dependencyUnorderedStringSet);

        printf("Target Name: %s\n", keyValuePair->key);
        printf("Dependencies: \n");
        for (PointerVectorIter p2 = dependencies.startIter; p2 != dependencies.endIter; ++p2) {
            // Print all the dependencies
            printf("\t%s\n", (char *) *p2);
        }
        cleanUpUStrSetStringVector(&dependencies);

        printf("Actions: \n");
        for (PointerVectorIter p2 = target->actionVector.startIter; p2 != target->actionVector.endIter; ++p2) {
            // Print all the actions
            TargetAction *action = (TargetAction *) *p2;

            printf("\t%s\n", action->command);
        }
    }
    cleanUpUStrMapPairVector(&targets);
}

void processBakefile(void) {
    // Select a target, and bake it if needed, else print if didn't need to
    Target *targetToBake = selectTargetToBake(&targetManager, &arguments);
    if (!bakeTarget(&targetManager, &commandManager, &arguments, targetToBake)) {
        PRINT_MESSAGE("Target already up to date: %s\n", targetToBake->targetName);
    }
}

void cleanUp(void) {
    cleanUpVariableManager(&variableManager);
    cleanUpTargetManager(&targetManager);
}