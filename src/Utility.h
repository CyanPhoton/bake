/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#ifndef BAKE_UTILITY_H
#define BAKE_UTILITY_H

#include <stdlib.h>

// Set to indicate that SETUP_MESSAGE_SOURCE has been used
#define BAKE_ERROR_SRC_DEFINED

// A string of the first argument, then path of the executable or sorts
extern const char *bakeErrorSrc;
// A macro to set the string
#define SETUP_MESSAGE_SOURCE(str) {bakeErrorSrc = str;}

// Set how to prepend the messages, depending on what was set
#ifdef BAKE_ERROR_SRC_DEFINED
#define BAKE_MESSAGE_PREPEND fprintf(stderr, "%s: ", bakeErrorSrc);
#else
#define BAKE_MESSAGE_PREPEND fputs("Bake Error: ", stderr);
#endif

/*
 * Prints error message to stderr if value evaluates true, and exits with failure
 */
#define FATAL_ERROR_IF(val, ...) \
    if (val) {\
        BAKE_MESSAGE_PREPEND\
        fprintf(stderr, __VA_ARGS__);\
        exit(EXIT_FAILURE);\
    }
/*
 * Prints error message to stderr and exits with failure
 */
#define FATAL_ERROR(...) \
    BAKE_MESSAGE_PREPEND\
    fprintf(stderr, __VA_ARGS__);\
    exit(EXIT_FAILURE);

/*
 * Prints template error messages for calls to the functions malloc(), realloc(),
 * strdup() or strndup() and terminates
 */
#define MALLOC_ERROR_CHECK(val)  if (val == NULL) {BAKE_MESSAGE_PREPEND fputs("Failed to allocate memory\n",  stderr); exit(EXIT_FAILURE);}
#define REALLOC_ERROR_CHECK(val) if (val == NULL) {BAKE_MESSAGE_PREPEND fputs("Failed to reallocate memory\n",stderr); exit(EXIT_FAILURE);}
#define STRDUP_ERROR_CHECK(val)  if (val == NULL) {BAKE_MESSAGE_PREPEND fputs("Failed to duplicate string\n", stderr); exit(EXIT_FAILURE);}

/*
 * Prints message to stdout, without exiting
 */
#define PRINT_MESSAGE(...) BAKE_MESSAGE_PREPEND fprintf(stdout, __VA_ARGS__);

#endif //BAKE_UTILITY_H
