/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#ifndef BAKE_POINTER_VECTOR_H
#define BAKE_POINTER_VECTOR_H

#include <stdlib.h>
#include <memory.h>
#include <stdio.h>

#include "../Utility.h"


/*  Pointer Vector
 *
 *  A pointer vector is a container designed to hold a list of pointers of any type.
 *  Where the list can automatically expand to fit the size needed as elements are
 *  added.
 *
 *  When given a pointer the container assumes responsibility for the memory,
 *  by assuming either it was already dynamically allocated, or by copying the
 *  memory into it's own dynamically allocated copy. So if you give it an already
 *  dynamically allocated pointer then you need to make sure to not free it yourself.
 *
 *  There are plenty of ways to improve this container, and more functionality that
 *  could be added but I have only developed what I need here.
 *
 */

/*
 * A couple constants that define the initial size (in elements) of the vector
 * when it is first created, as well as the size of the increase when it runs out
 * of room
 */
#ifdef BAKE_POINTER_VECTOR_CONSTANTS
#define INITIAL_VECTOR_SIZE 32
#define VECTOR_SIZE_STEP 16
#endif

/*
 * A typedef to just make it easier to iterate through a vector like:
 *
 * for (PointerVectorIter p = vec.startIter ; p != vec.endIter ; ++p){
 *
 * }
 *
 */
typedef void **PointerVectorIter;

/*
 * A struct that holds all the data needed for 1 struct, and acts as a sort of class.
 *
 * None of these values should be changed manually, only through passing it into one
 * of the functions here.
 *
 * But it can be used to access an element via:
 *
 * vec.startIter[x]
 *
 */
struct PointerVector {
    size_t allocatedSize;

    size_t size;
    PointerVectorIter startIter;
    PointerVectorIter endIter;
} typedef PointerVector;

/*
 * Just a function to initialise the fields of PointerVector struct
 */
void setupPointerVector(PointerVector *pointerVector);

/*
 * Adds a string into the vector, as well as copying the memory to have it's own copy
 */
void addVPElementCopyStr(PointerVector *pointerVector, const char *str); //TODO consider deleting

/*
 * Adds a pointer into the vector, as well as copying the memory to have it's own copy
 */
void addVPElementCopy(PointerVector *pointerVector, const void *pointer, size_t size);

/*
 * Adds a pointer to the vector, without copying the memory, but still assumes
 * responsibility to cleanup
 */
void addVPElement(PointerVector *pointerVector, const void *pointer);

/*
 * Manually increases the size of the memory allocation by the provided number of
 * elements, useful to prevent unnecessary repeated reallocation if adding a lot of
 * elements.
 */
void increaseVPAllocationSize(PointerVector *pointerVector, size_t increase);

/*
 * Cleans up all the data within the vector and the vector itself
 */
void cleanUpVectorPointer(PointerVector *pointerVector);

#endif //BAKE_POINTER_VECTOR_H
