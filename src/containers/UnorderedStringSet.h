/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#ifndef BAKE_UNORDERED_STRING_SET_H
#define BAKE_UNORDERED_STRING_SET_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <memory.h>

#include "../Utility.h"
#include "PointerVector.h"

/*  Unordered String Set
 *
 *  An unordered string set is a container designed to store a list of unique
 *  strings, with the use of hashing to speedup the access and allocation to
 *  hopefully near constant time on average, and linear in size at worst.
 *
 *  There are plenty of ways to improve this container, and more functionality that
 *  could be added but I have only developed what I need here.
 *
 */

/*
 * A constant which defines the number of bins in the unordered set, a higher bin
 * count allows for less overlap, but a higher (small but still higher) amount of
 * memory is used regardless of the actual number of elements stored
 *
 * The max value being due to the int16 used of 2^16 = 65536
 *
 */
#ifdef BAKE_UNORDERED_STRING_SET_CONSTANTS
    #define STR_SET_BIN_COUNT 512u
#endif

/*
 * A struct to represent a bin, with an array of strings and the size
 */
struct UStrSetBin {
    uint64_t count;
    char** strings;
} typedef UStrSetBin;

/*
 * A struct to hold all the data needed for an unordered string set, and acts as a
 * class of sorts.
 */
struct UStrSet {
    UStrSetBin* bins;
} typedef UStrSet;

/*
 * Simply a function to initialise the data in unordered string set
 */
void setupUStrSet(UStrSet *strSet);

/*
 * Adds a string to the unordered string set, copying the memory and taking
 * responsibility, if the string already exists nothing happens
 */
void addUStrSetString(UStrSet *strSet, const char *string);

/*
 * Returns true if the string is in the set, and false otherwise
 */
bool doesUStrSetContain(const UStrSet *strSet, const char *string); //TODO consider remove

/*
 * A function which takes a key, assumed to be a string and converts it to an
 * index in the range [0,STR_MAP_BIN_COUNT), it needs to be deterministic.
 * With the goal of as little overlap as possible to allow for fast lookup,
 * a 1 to 1 relationship is more desirable, but not really possible but a
 * many to 1 is the minimum, a 1 to many is not allowed. The reason a 1 to 1
 * is not really possible is that because to store enough pointers would require
 * (256 ^ x * 8)/2^30 GB for x characters, which becomes 32GB for 4 characters.
 *
 */
uint16_t hashKeyForUStrSet(const char *string);

/*
 * Gets a special pointer vector of all the strings in the set, but this vector
 * is NOT a deep copy, so you need to clean it up using the special function bellow.
 */
PointerVector getUStrSetStringVector(const UStrSet *strSet);

/*
 * A special clean up function to clean up the vector returned above, need due to
 * it not being a deep copy and the way Pointer Vector works
 */
void cleanUpUStrSetStringVector(PointerVector* pointerVector);

/*
 * A function to simply free all the memory 'owned' by the unordered string set
 */
void cleanUpUStrSet(UStrSet *strSet);

#endif //BAKE_UNORDERED_STRING_SET_H
