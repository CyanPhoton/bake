/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#define BAKE_UNORDERED_STRING_SET_CONSTANTS
#include "UnorderedStringSet.h"

void setupUStrSet(UStrSet *strSet) {
    strSet->bins = calloc(STR_SET_BIN_COUNT, sizeof(UStrSetBin));
}

void addUStrSetString(UStrSet *strSet, const char *string) {
    // Get the hashed index
    uint16_t hash = hashKeyForUStrSet(string);

    // Get the particular bin
    UStrSetBin *bin = strSet->bins + hash;

    if (bin->count == 0) {
        // Bin is empty, so just allocate room for count, and key-value struct

        bin->strings = malloc(sizeof(char *));
        MALLOC_ERROR_CHECK(bin);
        bin->count = 1;
    } else {
        // Else already some values

        // Iterate through each value, looking for the same string
        for (uint64_t i = 0; i < bin->count; ++i) {
            if (strcmp(string, bin->strings[i]) == 0) {
                // Since the string should only be there once, and it is there
                // already done and just return
                return; // Success
            }
        }
        // The string isn't already in there, so allocate room

        ++bin->count;
        bin->strings = realloc(bin->strings, sizeof(char *) * bin->count);
        REALLOC_ERROR_CHECK(bin->strings);
    }
    // Bin was empty or didn't contain the string, so copy in new one

    bin->strings[bin->count - 1] = strdup(string);
    STRDUP_ERROR_CHECK(bin->strings[bin->count - 1]);
}

bool doesUStrSetContain(const UStrSet *strSet, const char *string) {
    // Get the hashed index
    uint16_t hash = hashKeyForUStrSet(string);

    // Get the particular bin
    UStrSetBin *bin = strSet->bins + hash;

    if (bin->count == 0) {
        // Bin is empty, so not there
        return false;
    }

    // Else already some values

    // Iterate through each value, looking for the same string
    for (uint64_t i = 0; i < bin->count; ++i) {
        if (strcmp(string, bin->strings[i]) == 0) {
            // Since it is there just return true
            return true;
        }
    }
    // The string isn't already in there, return false

    return false;
}

uint16_t hashKeyForUStrSet(const char *string) {
    // Idk if this is good, but it seems to work
    // TODO research hashing algorithms?

    uint16_t total = 1;
    for (const char *c = string; *c != '\0'; ++c) {
        total += (c - string + 1);
        total *= (int) (*c);
    }

    // raps around to fit range [0,STR_SET_BIN_COUNT)
    total %= STR_SET_BIN_COUNT;
    return total;
}

PointerVector getUStrSetStringVector(const UStrSet *strSet) {
    //Create vector to store data in
    PointerVector result;
    setupPointerVector(&result);

    // Iterate though each bin
    for (uint16_t i = 0; i < STR_SET_BIN_COUNT; ++i) {
        UStrSetBin *bin = strSet->bins + i;
        if (bin->count > 0) {
            // Bin count is > 0

            // Increase allocation by number going to be added (maybe)
            increaseVPAllocationSize(&result, bin->count);

            for (uint64_t item = 0; item < bin->count; ++item) {
                addVPElement(&result, bin->strings[item]);
            }
        }
    }

    return result;
}

void cleanUpUStrSetStringVector(PointerVector *pointerVector) {
    free(pointerVector->startIter);
}

void cleanUpUStrSet(UStrSet *strSet) {
    // Iterate though each bin
    for (uint16_t i = 0; i < STR_SET_BIN_COUNT; ++i) {
        UStrSetBin *bin = strSet->bins + i;
        if (bin->count > 0) {
            // Bin count is > 0

            for (uint64_t item = 0; item < bin->count; ++item) {
                free(bin->strings[item]);
            }

            free(bin->strings);
        }
    }

    free(strSet->bins);
}
