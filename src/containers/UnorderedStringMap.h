/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#ifndef BAKE_UNORDERED_STRING_MAP_H
#define BAKE_UNORDERED_STRING_MAP_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <memory.h>

#include "../Utility.h"
#include "PointerVector.h"

/*  Unordered String Map
 *
 *  An unordered string map is a container designed to facilitate one to one
 *  key-value relationships and the looking up of values via the key, with the use
 *  of hashing to speedup the access and allocation to hopefully near constant time
 *  on average, and linear in size at worst.
 *
 *  There are plenty of ways to improve this container, and more functionality that
 *  could be added but I have only developed what I need here.
 *
 */

/*
 * A constant which defines the number of bins in the unordered map, a higher bin
 * count allows for less overlap, but a higher (small but still higher) amount of
 * memory is used regardless of the actual number of elements stored
 *
 * The max value being due to the int16 used of 2^16 = 65536
 *
 */
#ifdef BAKE_UNORDERED_STRING_MAP_CONSTANTS
#define STR_MAP_BIN_COUNT 1024u
#endif

/*
 * Simple type to allow the appearance on not being limited to char's and unlike
 * void allow for pointer arithmetic
 */
typedef char byte;

/*
 * A struct to represent a key-value pair in the map
 */
struct UStrMapKeyValuePair {
    char *key;
    byte *valuePointer;
} typedef UStrMapKeyValuePair;

/*
 * A struct to represent a bin, with an array of ket-value pairs and the size
 */
struct UStrMapBin {
    uint64_t count;
    UStrMapKeyValuePair *keyValuePairs;
} typedef UStrMapBin;

/*
 * A struct to hold all the data needed for an unordered string map, and acts as a
 * class of sorts.
 */
struct UStrMap {
    UStrMapBin *bins;
} typedef UStrMap;

/*
 * Simply a function to initialise the data in unordered string map
 */
void setupUStrMap(UStrMap *strMap);

/*
 * Sets the value of a key in the map, allocating and copying the data in, taking
 * responsibility to free the memory
 */
void setUStrMapElement(UStrMap *strMap, const char *key, const void *value, size_t size);

/*
 * Simply a version of setUStrMapElement designed for strings, so the size param
 * is not required
 */
void setUStrMapElementStr(UStrMap *strMap, const char *key, const char *value);

/*
 * Returns a pointer to the location in memory of the value associated with the
 * key provided, or NULL if there is no value
 */
byte *getUStrMapValue(const UStrMap *strMap, const char *key);

/*
 * Returns a pointer to the location in memory of the key-value pair with the key
 * provided, or NULL if there is no value
 */
UStrMapKeyValuePair *getUStrMapKeyValue(const UStrMap *strMap, const char *key);

/*
 * A function which takes a key, assumed to be a string and converts it to an
 * index in the range [0,STR_MAP_BIN_COUNT), it needs to be deterministic.
 * With the goal of as little overlap as possible to allow for fast lookup,
 * a 1 to 1 relationship is more desirable, but not really possible but a
 * many to 1 is the minimum, a 1 to many is not allowed. The reason a 1 to 1
 * is not really possible is that because to store enough pointers would require
 * (256 ^ x * 8)/2^30 GB for x characters, which becomes 32GB for 4 characters.
 *
 */
uint16_t hashKeyForUStrMap(const char *key);

/*
 * Gets a special pointer vector of all the key-value pairs in the map, but this vector
 * is NOT a deep copy, so you need to clean it up using the special function bellow.
 */
PointerVector getUStrMapPairVector(const UStrMap *strMap);

/*
 * A special clean up function to clean up the vector returned above, need due to
 * it not being a deep copy and the way Pointer Vector works
 */
void cleanUpUStrMapPairVector(PointerVector *pointerVector);

/*
 * A function to simply free all the memory 'owned' by the unordered string map
 */
void cleanUpUStrMap(UStrMap *strMap);

#endif //BAKE_UNORDERED_STRING_MAP_H
