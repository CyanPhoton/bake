/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#define BAKE_POINTER_VECTOR_CONSTANTS
#include "PointerVector.h"

void setupPointerVector(PointerVector *pointerVector) {
    memset(pointerVector, 0, sizeof(PointerVector));
}

void addVPElementCopyStr(PointerVector *pointerVector, const char *str) {
    addVPElementCopy(pointerVector, str, strlen(str) + 1);
}

void addVPElementCopy(PointerVector *pointerVector, const void *pointer, size_t size) {
    // Add element
    addVPElement(pointerVector, pointer);

    // Copy over data
    pointerVector->endIter[-1] = malloc(size);
    MALLOC_ERROR_CHECK(pointerVector->endIter[-1]);
    memcpy(pointerVector->endIter[-1], pointer, size);
}

void addVPElement(PointerVector *pointerVector, const void *pointer) {
    if (pointerVector->allocatedSize == 0) {
        // If empty increase to default size

        pointerVector->allocatedSize = INITIAL_VECTOR_SIZE;
        pointerVector->size = 0;
        pointerVector->startIter = malloc(sizeof(void *) * INITIAL_VECTOR_SIZE);
        MALLOC_ERROR_CHECK(pointerVector->startIter);
        pointerVector->endIter = pointerVector->startIter;

    } else if (pointerVector->size + 1 > pointerVector->allocatedSize) {
        // If not empty but not enough room, just increase by default step size and rest end iter

        pointerVector->allocatedSize += VECTOR_SIZE_STEP;
        pointerVector->startIter = realloc(pointerVector->startIter, sizeof(void *) * pointerVector->allocatedSize);
        REALLOC_ERROR_CHECK(pointerVector->startIter);
        pointerVector->endIter = pointerVector->startIter + pointerVector->size;
    }

    // Increment size and end
    ++pointerVector->size;
    ++pointerVector->endIter;

    // Set pointer
    pointerVector->endIter[-1] = (void *) pointer;
}

void increaseVPAllocationSize(PointerVector *pointerVector, size_t increase) {
    // Just increase allocation and reset the end iter
    pointerVector->allocatedSize += increase;
    pointerVector->startIter = realloc(pointerVector->startIter, sizeof(void *) * pointerVector->allocatedSize);
    REALLOC_ERROR_CHECK(pointerVector->startIter);
    pointerVector->endIter = pointerVector->startIter + pointerVector->size;
}

void cleanUpVectorPointer(PointerVector *pointerVector) {
    // Free each pointer stored
    for (PointerVectorIter p = pointerVector->startIter; p != pointerVector->endIter; ++p) {
        free(*p);
    }

    // Free vector itself
    free(pointerVector->startIter);
}
