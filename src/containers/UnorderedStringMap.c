/* CITS2002 Project 2018
 * Name(s):             Matthew Chidlow,  Mitchell Giles
 * Student number(s):   22482835,         22490361
 */

#define BAKE_UNORDERED_STRING_MAP_CONSTANTS
#include "UnorderedStringMap.h"

void setupUStrMap(UStrMap *strMap) {
    strMap->bins = calloc(STR_MAP_BIN_COUNT, sizeof(UStrMapBin));
}

void setUStrMapElementStr(UStrMap *strMap, const char *key, const char *value) {
    // Simply call the more general one using strlen to get size
    setUStrMapElement(strMap, key, value, strlen(value) + 1);
}

void setUStrMapElement(UStrMap *strMap, const char *key, const void *value, const size_t size) {
    // Get the hashed index
    uint16_t hash = hashKeyForUStrMap(key);

    // Get the particular bin
    UStrMapBin *bin = strMap->bins + hash;

    if (bin->count == 0) {
        // Bin is empty, so just allocate room for count, and key-value struct

        bin->keyValuePairs = malloc(sizeof(UStrMapKeyValuePair));
        MALLOC_ERROR_CHECK(bin);
        bin->count = 1;
    } else {
        // Else already some values

        // Iterate through each value, looking for pairs with the same key
        for (uint64_t i = 0; i < bin->count; ++i) {
            if (strcmp(key, bin->keyValuePairs[i].key) == 0) {
                // Since the key-value relation is 1 to 1, if a key already exits
                // Then the old value is remove and new one replaces it

                free(bin->keyValuePairs[i].valuePointer);
                bin->keyValuePairs[i].valuePointer = malloc(size);
                MALLOC_ERROR_CHECK(bin->keyValuePairs[i].valuePointer);
                memcpy(bin->keyValuePairs[i].valuePointer, value, size);
                return; // Success
            }
        }
        // The key doesn't already exist and, so increase allocation

        ++bin->count;
        bin->keyValuePairs = realloc(bin->keyValuePairs, sizeof(UStrMapKeyValuePair) * bin->count);
        REALLOC_ERROR_CHECK(bin->keyValuePairs);
    }
    // Bin was empty or NULL, or reallocation was required, so now store the new Value

    // Gets last value's pointer
    UStrMapKeyValuePair *newPair = bin->keyValuePairs + (bin->count - 1);

    // Duplicate key string
    newPair->key = strdup(key);
    STRDUP_ERROR_CHECK(newPair->key);

    // Make room for value data
    newPair->valuePointer = malloc(size);
    MALLOC_ERROR_CHECK(newPair->valuePointer);

    // Copy in value data
    memcpy(newPair->valuePointer, value, size);
}

byte *getUStrMapValue(const UStrMap *strMap, const char *key) {
    // Simply get pair and select value if valid
    UStrMapKeyValuePair *pair = getUStrMapKeyValue(strMap, key);
    if (pair == NULL) {
        return NULL;
    }
    return pair->valuePointer;
}

UStrMapKeyValuePair *getUStrMapKeyValue(const UStrMap *strMap, const char *key) {
    // Get the bin index from hash
    uint16_t hash = hashKeyForUStrMap(key);

    // Get pointer to the bin
    UStrMapBin *bin = strMap->bins + hash;

    if (bin->count == 0) {
        // Obviously not there if count is 0
        return NULL;
    }

    for (uint64_t i = 0; i < bin->count; ++i) {
        if (bin->keyValuePairs[i].key == NULL) {
            continue;
        }
        if (strcmp(key, bin->keyValuePairs[i].key) == 0) {
            // If keys are equal just return pointer to pair
            return bin->keyValuePairs + i;
        }
    }

    // No pair found with same key, so not present
    return NULL;
}

uint16_t hashKeyForUStrMap(const char *key) {
    // Idk if this is good, but it seems to work
    // TODO research hashing algorithms?

    uint16_t total = 1;
    for (const char *c = key; *c != '\0'; ++c) {
        total += (c - key + 1);
        total *= (int) (*c);
    }

    // raps around to fit range [0,STR_MAP_BIN_COUNT)
    total %= STR_MAP_BIN_COUNT;
    return total;
}

PointerVector getUStrMapPairVector(const UStrMap *strMap) {
    // Create vector to store data in
    PointerVector result;
    setupPointerVector(&result);

    // Iterate though each bin
    for (uint16_t i = 0; i < STR_MAP_BIN_COUNT; ++i) {
        UStrMapBin *bin = strMap->bins + i;
        if (bin->count > 0) {
            // Bin count is > 0

            // Increase allocation by number going to be added (maybe)
            increaseVPAllocationSize(&result, bin->count);

            for (uint64_t item = 0; item < bin->count; ++item) {
                if (bin->keyValuePairs[item].key != NULL) {
                    // If pair valid add it to vector without copy
                    addVPElement(&result, bin->keyValuePairs + item);
                }
            }
        }
    }

    return result;
}

void cleanUpUStrMapPairVector(PointerVector *pointerVector) {
    free(pointerVector->startIter);
}

void cleanUpUStrMap(UStrMap *strMap) {
    // Iterate though each bin
    for (uint16_t i = 0; i < STR_MAP_BIN_COUNT; ++i) {
        UStrMapBin *bin = strMap->bins + i;
        if (bin->count > 0) {
            // Bin count is > 0

            for (uint64_t item = 0; item < bin->count; ++item) {
                if (bin->keyValuePairs[item].key != NULL) {
                    // Free each key and valuePointer

                    free(bin->keyValuePairs[item].key);
                    free(bin->keyValuePairs[item].valuePointer);
                }
            }

            free(bin->keyValuePairs);
        }
    }

    free(strMap->bins);
}
